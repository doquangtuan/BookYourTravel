<?php

get_header();
BookYourTravel_Theme_Utils::breadcrumbs();
get_sidebar('under-header');

global $post, $tour_price, $bookyourtravel_theme_globals, $tour_date_from, $current_user, $tour_obj, $entity_obj, $default_tour_tabs, $score_out_of_10, $bookyourtravel_tour_helper, $bookyourtravel_theme_of_custom;

$enable_extra_items = $bookyourtravel_theme_globals->enable_extra_items();
$enable_reviews = $bookyourtravel_theme_globals->enable_reviews();
$enable_tours = $bookyourtravel_theme_globals->enable_tours();
$price_decimal_places = $bookyourtravel_theme_globals->get_price_decimal_places();
$tour_extra_fields = $bookyourtravel_theme_globals->get_tour_extra_fields();
$tab_array = $bookyourtravel_theme_globals->get_tour_tabs();

$price_decimal_places = $bookyourtravel_theme_globals->get_price_decimal_places();
$default_currency_symbol = $bookyourtravel_theme_globals->get_default_currency_symbol();
$show_currency_symbol_after = $bookyourtravel_theme_globals->show_currency_symbol_after();
$light_slider_pause_between_slides = $bookyourtravel_theme_globals->get_light_slider_pause_between_slides();

if ( have_posts() ) {

	the_post();

	$tour_obj = new BookYourTravel_Tour($post);


	$tour_id = $tour_obj->get_id();
	$entity_obj = $tour_obj;
	$tour_map_code = $tour_obj->get_custom_field( 'map_code' );
	$tour_locations = $tour_obj->get_locations();
	$tour_is_reservation_only = $tour_obj->get_is_reservation_only();

	$tour_date_from = date('Y-m-d', strtotime("+0 day", time()));
	$tour_date_from_year = date('Y', strtotime("+0 day", time()));
	$tour_date_from_month = date('n', strtotime("+0 day", time()));

	$tour_price = $bookyourtravel_tour_helper->get_tour_min_price($tour_id);
	$tour_locations = $tour_obj->get_locations();$tour_location_title = '';
	if ($tour_locations && count($tour_locations) > 0) {
		foreach ($tour_locations as $location_id) {
			$location_obj = new BookYourTravel_Location((int)$location_id);
			$location_title = $location_obj->get_title();		$tour_location_title .= $location_title . ', ';
		}
	}
	$tour_location_title = rtrim($tour_location_title, ', ');
	$image = wp_get_attachment_image_src( get_post_thumbnail_id($tour_obj->id), 'single-post-thumbnail' );

?>
<div class="row">
	<script>
		window.postType = 'tour';
		window.pauseBetweenSlides = <?php echo $light_slider_pause_between_slides * 1000; ?>;
	</script>
<?php
	if ($enable_reviews) {
		get_template_part('includes/parts/review', 'form');
	}
	get_template_part('includes/parts/inquiry', 'form');
?>
<!--tour three-fourth content-->
<section class="three-fourth">
	<?php
	get_template_part('includes/parts/tour', 'booking-form');
	get_template_part('includes/parts/tour', 'confirmation-form');
	?>
	<script>
		window.bookingFormStartDateError = <?php echo json_encode(pll__('Please select a valid start date!', 'bookyourtravel')); ?>;
		window.startDate = null;
		window.formSingleError = <?php echo json_encode(pll__('You failed to provide 1 field. It has been highlighted below.', 'bookyourtravel')); ?>;
		window.formMultipleError = <?php echo json_encode(pll__('You failed to provide {0} fields. They have been highlighted below.', 'bookyourtravel'));  ?>;
		window.tourId = <?php echo $tour_obj->get_id(); ?>;
		window.tourIsPricePerGroup = <?php echo $tour_obj->get_is_price_per_group(); ?>;
		window.tourDateFrom = <?php echo json_encode($tour_date_from); ?>;
		window.tourTitle = <?php echo json_encode($tour_obj->get_title()); ?>;
		window.currentMonth = <?php echo json_encode(date_i18n('n')); ?>;
		window.currentYear = <?php echo json_encode( date_i18n('Y')); ?>;
		window.currentDay = <?php echo json_encode( date_i18n('j')); ?>;
		window.tourIsReservationOnly = <?php echo (int)$tour_is_reservation_only; ?>;
		window.enableExtraItems = <?php echo json_encode($enable_extra_items); ?>;
		window.showPriceBreakdownLabel = <?php echo json_encode(pll__('Show price breakdown', 'bookyourtravel')); ?>;
		window.hidePriceBreakdownLabel = <?php echo json_encode(pll__('Hide price breakdown', 'bookyourtravel')); ?>;
		window.dateLabel = <?php echo json_encode(pll__('Date', 'bookyourtravel')); ?>;
		window.itemLabel = <?php echo json_encode(pll__('Item', 'bookyourtravel')); ?>;
		window.priceLabel = <?php echo json_encode(pll__('Price', 'bookyourtravel')); ?>;
		window.pricedPerDayPerPersonLabel = <?php echo json_encode(pll__('priced per day, per person', 'bookyourtravel')); ?>;
		window.pricedPerDayLabel = <?php echo json_encode(pll__('priced per day', 'bookyourtravel')); ?>;
		window.pricedPerPersonLabel = <?php echo json_encode(pll__('priced per person', 'bookyourtravel')); ?>;
		window.pricePerAdultLabel = <?php echo json_encode(pll__('Price per adult', 'bookyourtravel')); ?>;
		window.pricePerPersonLabel = <?php echo json_encode(pll__('Price per person', 'bookyourtravel')); ?>;
		window.adultCountLabel = <?php echo json_encode(pll__('Adults', 'bookyourtravel')); ?>;
		window.childCountLabel = <?php echo json_encode(pll__('Children', 'bookyourtravel')); ?>;
		window.pricePerChildLabel = <?php echo json_encode(pll__('Price per child', 'bookyourtravel')); ?>;
		window.pricePerDayLabel = <?php echo json_encode(pll__('Price per day', 'bookyourtravel')); ?>;
		window.extraItemsPriceTotalLabel = <?php echo json_encode(pll__('Extra items total price', 'bookyourtravel')); ?>;
		window.priceTotalLabel = <?php echo json_encode(pll__('Total price', 'bookyourtravel')); ?>;
	</script>
	<?php $tour_obj->render_image_gallery(); ?>
	<!--inner navigation-->
	<nav class="inner-nav">
		<ul>			<li class="info_tour"><a href="#info_tour" title="<?php pll_e('Information Tour'); ?>"><?php pll_e('Information Tour'); ?></a></li>
			<?php
			do_action( 'bookyourtravel_show_single_tour_tab_items_before' );
			$first_display_tab = '';
			$i = 0;
			if (is_array($tab_array) && count($tab_array) > 0) {
				foreach ($tab_array as $tab) {
					if (!isset($tab['hide']) || $tab['hide'] != '1') {

						$tab_label = '';
						if (isset($tab['label'])) {
							$tab_label = $tab['label'];
							$tab_label = $bookyourtravel_theme_of_custom->get_translated_dynamic_string($bookyourtravel_theme_of_custom->get_option_id_context('tour_tabs') . ' ' . $tab['label'], $tab_label);
						}

						if($i==0)
							$first_display_tab = $tab['id'];
						if ($tab['id'] == 'reviews' && $enable_reviews) {
							BookYourTravel_Theme_Utils::render_tab("tour", $tab['id'], '',  '<a href="#' . $tab['id'] . '" title="' . $tab_label . '">' . $tab_label . '</a>');
						} elseif ($tab['id'] == 'map' && !empty($tour_map_code)) {
							BookYourTravel_Theme_Utils::render_tab("tour", $tab['id'], '',  '<a href="#' . $tab['id'] . '" title="' . $tab_label . '">' . $tab_label . '</a>');
						} elseif ($tab['id'] == 'locations') {
							if ($tour_locations && count($tour_locations) > 0)
								BookYourTravel_Theme_Utils::render_tab("tour", $tab['id'], '',  '<a href="#' . $tab['id'] . '" title="' . $tab_label . '">' . $tab_label . '</a>');
						} elseif ($tab['id'] == 'description' || $tab['id'] == 'availability') {
							BookYourTravel_Theme_Utils::render_tab("tour", $tab['id'], '',  '<a href="#' . $tab['id'] . '" title="' . $tab_label . '">' . $tab_label . '</a>');
						} elseif ($tab['id'] == 'note') {
							BookYourTravel_Theme_Utils::render_tab("tour", $tab['id'], '',  '<a href="#' . $tab['id'] . '" title="' . $tab_label . '">' . $tab_label . '</a>');
						} else {
							$all_empty_fields = BookYourTravel_Theme_Utils::are_tab_fields_empty('tour_extra_fields', $tour_extra_fields, $tab['id'], $tour_obj);

							if (!$all_empty_fields) {
								BookYourTravel_Theme_Utils::render_tab("tour", $tab['id'], '',  '<a href="#' . $tab['id'] . '" title="' . $tab_label . '">' . $tab_label . '</a>');
							}
						}

						$i++;
					}
				}
			}
			do_action( 'bookyourtravel_show_single_tour_tab_items_after' );
			?>
		</ul>
	</nav>
	<!--//inner navigation-->
	<?php do_action( 'bookyourtravel_show_single_tour_tab_content_before' ); ?>	<!--infor-tour-->		<section id="info_tour" class="tab-content initial">			<article class="tour-details">				<img src="<?php echo $image[0] ;?>" />				<h1><?php echo $tour_obj->get_title(); ?></h1>				<span class="address"><?php echo $tour_location_title; ?></span>				<?php if ($score_out_of_10 > 0) { ?>				<span class="rating"><?php echo $score_out_of_10; ?> / 10</span>				<?php } ?>								<?php if ($tour_price > 0) { ?>				<div class="price">					<?php esc_html_e('Price from ', 'bookyourtravel'); ?>					<em>					<?php if (!$show_currency_symbol_after) { ?>					<span class="curr"><?php echo esc_html($default_currency_symbol); ?></span>					<span class="amount"><?php echo number_format_i18n( $tour_price, $price_decimal_places ); ?></span>					<?php } else { ?>					<span class="amount"><?php echo number_format_i18n( $tour_price, $price_decimal_places ); ?></span>					<span class="curr"><?php echo esc_html($default_currency_symbol); ?></span>					<?php } ?>					</em>				</div>				<?php } ?>								<?php BookYourTravel_Theme_Utils::render_field("description", "", "", BookYourTravel_Theme_Utils::strip_tags_and_shorten($tour_obj->get_description(), 100), "", true); ?>								<?php				$tags = $tour_obj->get_tags();				if (count($tags) > 0) {?>				<div class="tags">					<ul>						<?php							foreach ($tags as $tag) {								$tag_link = get_term_link( (int)$tag->term_id, 'tour_tag' );								echo '<li><a href="' . $tag_link . '">' . $tag->name . '</a></li>';							}						?>											</ul>				</div>				<?php } ?>								<?php 				if ($enable_reviews) {					$reviews_by_current_user_query = $bookyourtravel_review_helper->list_reviews($tour_obj->get_base_id(), $current_user->ID);					if (!$reviews_by_current_user_query->have_posts() && is_user_logged_in()) {						BookYourTravel_Theme_Utils::render_link_button("#", "gradient-button right leave-review review-tour", "", esc_html__('Leave a review', 'bookyourtravel'));					} 					?>					<p class="review-form-thank-you" style="display:none;">					<?php esc_html_e('Thank you for submitting a review.', 'bookyourtravel'); ?>					</p>					<?php				}				/*if (!$tour_obj->get_custom_field('hide_inquiry_form')) {					BookYourTravel_Theme_Utils::render_link_button("#", "gradient-button right contact-tour", "", esc_html__('Send inquiry', 'bookyourtravel'));					?>					<p class="inquiry-form-thank-you" style="display:none;">					<?php esc_html_e('Thank you for submitting an inquiry. We will get back to you as soon as we can.', 'bookyourtravel'); ?>					</p>					<?php				} */?>			</article>	</section>
	<!--description-->
	<section id="description" class="tab-content description">
		<article>
			<?php do_action( 'bookyourtravel_show_single_tour_description_before' ); ?>
			<?php BookYourTravel_Theme_Utils::render_field("text-wrap", "", "", $tour_obj->get_description(), esc_html__('General', 'bookyourtravel')); ?>
			<?php BookYourTravel_Theme_Utils::render_tab_extra_fields('tour_extra_fields', $tour_extra_fields, 'description', $tour_obj); ?>
			<?php do_action( 'bookyourtravel_show_single_tour_description_after' ); ?>
		</article>
	</section>
	<!--//description-->
	<!--availability-->
	<section id="availability" class="tab-content <?php echo $first_display_tab == 'availability' ? 'initial' : ''; ?>">
		<article>
			<?php do_action( 'bookyourtravel_show_single_tour_availability_before' ); ?>
			<h2><?php pll_e('Available departures'); ?></h2>
			<?php BookYourTravel_Theme_Utils::render_field("text-wrap", "", "", $tour_obj->get_custom_field('availability_text'), '', false, true); ?>
			<form id="launch-tour-booking" action="#" method="POST">
				<?php
				$type_day_of_week_indexes = $tour_obj->get_type_day_of_week_indexes();

				$year  = date('Y', strtotime($tour_date_from));
				$month  = date('m', strtotime($tour_date_from));
				$schedule_entries = $bookyourtravel_tour_helper->list_available_tour_schedule_entries($tour_obj->get_id(), $tour_date_from, $year, $month, $tour_obj->get_type_is_repeated(), $type_day_of_week_indexes);


				if (count($schedule_entries) > 0) { ?>

					<div class="date-start">
						<div class="text-wrap">
							<h3><?php pll_e('Ngày khởi hành'); ?></h3>
							<select name="date-start" id="date-start">
								<?php for ($i=0;$i<count($schedule_entries);$i++) {
									$currSchedule = $schedule_entries[$i];
									$date = date_create($currSchedule->tour_date);
									$dateFormated = date_format($date, 'd-m-Y');
									$dateValueFormated = date_format($date, 'Y-m-d');
								?>
									<option value="<?php echo $dateValueFormated; ?>"><?php echo $dateFormated; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="booking_form_controls_holder">
						<table class="extraitems responsive">
							<thead>
								<tr>
									<th colspan="3"><?php pll_e('Tour Price'); ?></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<label for="booking_form_adults"><?php pll_e('Adults') ?></label>
									</td>
									<td>
										<em>
											<span class="amount">
												<?php
													for($i=0; $i<count($schedule_entries); $i++) {
														$currSchedule2 = $schedule_entries[$i];
													}
													$strNum = round($currSchedule2->price, 2);
													$len = strlen($strNum);
													$counter = 3;
													$result = "";
													while ($len - $counter >= 0)
													{
														$con = substr($strNum, $len - $counter , 3);
														$result = ','.$con.$result;
														$counter+= 3;
													}
														$con = substr($strNum, 0 , 3 - ($counter - $len) );
														$result = $con.$result;
													if(substr($result,0,1)==','){
														$result=substr($result,1,$len+1);
													}

													echo $result;

												?>
											</span>
											<span class="curr">VND</span>
										</em>
									</td>
									<td>
											<div class="f-item">
												<select class="dynamic_control" id="booking_form_adults" name="booking_form_adults"></select>
											</div>
									</td>
								</tr>
								<tr>
									<td>
										<label for="booking_form_adults"><?php pll_e('Children') ?></label>
									</td>
									<td>
										<em>
											<span class="amount">
												<?php
													for($i=0; $i<count($schedule_entries); $i++) {
														$currSchedule2 = $schedule_entries[$i];
													}
													$strNum = round($currSchedule2->price_child, 2);
													$len = strlen($strNum);
													$counter = 3;
													$result = "";
													while ($len - $counter >= 0)
													{
														$con = substr($strNum, $len - $counter , 3);
														$result = ','.$con.$result;
														$counter+= 3;
													}
														$con = substr($strNum, 0 , 3 - ($counter - $len) );
														$result = $con.$result;
													if(substr($result,0,1)==','){
														$result=substr($result,1,$len+1);
													}

													echo $result;
												?>
											</span>
											<span class="curr">VND</span>
										</em>
									</td>

									<td>
											<div class="f-item booking_form_children">
												<select class="dynamic_control" id="booking_form_children" name="booking_form_children"></select>
											</div>
									</td>
								</tr>
							</tbody>
						</table>

						<?php if ($enable_extra_items) {
						global $bookyourtravel_extra_item_helper;

						$extra_items = $bookyourtravel_extra_item_helper->list_extra_items_by_post_type('tour', array($tour_obj->get_type_id()), $tour_obj->get_tag_ids());
						if (count($extra_items) > 0) {
						?>
						<div class="text-wrap price_row extra_items_row">
							<h3><?php pll_e('Extra items') ?></h3>
							<p><?php pll_e('Please select the extra items you wish to be included on your tour using the controls you see below.') ?></p>

							<table class="extraitems responsive">
								<thead>
									<tr>
										<th><?php pll_e('Item'); ?></th>
										<th><?php pll_e('Price'); ?></th>
										<th><?php pll_e('Quantity'); ?></th>
									</tr>
								</thead>
								<tbody>
									<script>
										window.requiredExtraItems = [];
									</script>
									<?php
										foreach ($extra_items as $extra_item) {
											$tour_extra_item_obj = new BookYourTravel_Extra_Item($extra_item);
											$item_teaser = BookYourTravel_Theme_Utils::strip_tags_and_shorten_by_words($tour_extra_item_obj->get_content(), 20);
											$max_allowed = $tour_extra_item_obj->get_custom_field('_extra_item_max_allowed', false);
											$item_price =$tour_extra_item_obj->get_custom_field('_extra_item_price', false);
											$item_is_required = intval($tour_extra_item_obj->get_custom_field('_extra_item_is_required', false));

											if ($max_allowed > 0) {
												$starting_index = $item_is_required ? 1 : 0;

												if ($item_is_required) {
												?>
												<script>
													window.requiredExtraItems.push(<?php echo $extra_item->ID; ?>);
												</script>
												<?php
												}
										?>
										<tr>
											<td>
												<span id="extra_item_title_<?php echo esc_attr($extra_item->ID); ?>"><?php echo esc_html($extra_item->post_title); ?></span>
												<?php if (!empty($item_teaser)) { ?>
												<i>
												<?php
												$allowed_tags = BookYourTravel_Theme_Utils::get_allowed_content_tags_array();
												echo wp_kses($item_teaser, $allowed_tags);
												?>
												</i>
												<?php } ?>
											</td>
											<td>
												<em>
													<?php if (!$show_currency_symbol_after) { ?>
													<span class="curr"><?php echo esc_html($default_currency_symbol); ?></span>
													<span class="amount"><?php echo number_format_i18n( $item_price, $price_decimal_places ); ?></span>
													<?php } else { ?>
													<span class="amount"><?php echo number_format_i18n( $item_price, $price_decimal_places ); ?></span>
													<span class="curr"><?php echo esc_html($default_currency_symbol); ?></span>
													<?php } ?>
													<input type="hidden" value="<?php echo esc_attr($item_price); ?>" name="extra_item_price_<?php echo esc_attr($extra_item->ID); ?>" id="extra_item_price_<?php echo esc_attr($extra_item->ID); ?>" />
													<input type="hidden" value="<?php echo esc_attr($item_price_per_person); ?>" name="extra_item_price_per_person_<?php echo esc_attr($extra_item->ID); ?>" id="extra_item_price_per_person_<?php echo esc_attr($extra_item->ID); ?>" />
													<input type="hidden" value="<?php echo esc_attr($item_price_per_day); ?>" name="extra_item_price_per_day_<?php echo esc_attr($extra_item->ID); ?>" id="extra_item_price_per_day_<?php echo esc_attr($extra_item->ID); ?>" />
												</em>
											</td>
											<td>
												<select class="extra_item_quantity dynamic_control" name="extra_item_quantity_<?php echo esc_attr($extra_item->ID); ?>" id="extra_item_quantity_<?php echo esc_attr($extra_item->ID); ?>">
													<?php for ($i=$starting_index;$i<=$max_allowed;$i++) {?>
													<option value="<?php echo esc_attr($i); ?>"><?php echo esc_html($i); ?></option>
													<?php } ?>
												</select>
											</td>
										</tr>
										<?php
											}
										}
									?>
								</tbody>
								<tfoot></tfoot>
							</table>
						</div>
						<?php }
						}
						?>

						<div class="text-wrap dates_row">
							<h3><?php pll_e('Summary'); ?></h3>
							<p><?php pll_e('The summary of your tour booking is shown below.'); ?></p>

							<table class="summary responsive">
								<tbody>
									<tr>
										<th><?php pll_e('Start date'); ?></th>
										<td>
											<span id="start_date_span"></span>
											<input type="hidden" name="start_date" id="start_date" value="" />
										</td>
									</tr>
									<!-- <tr>
										<th><?php pll_e('Tour duration (days)') ?></th>
										<td>
											<span id="duration_days_span"></span>
											<input type="hidden" name="duration_days" id="duration_days" value="" />
										</td>
									</tr> -->
									<tr class=" people_count_div" style="display:none">
										<th>
											<?php pll_e('People') ?>
										</th>
										<td>
											<span class="people_text">1</span>
										</td>
									</tr>
									<tr class=" adult_count_div">
										<th>
											<?php pll_e('Adults') ?>
										</th>
										<td>
											<span class="adults_text">1</span>
										</td>
									</tr>
									<tr class=" children_count_div">
										<th>
											<?php pll_e('Children') ?>
										</th>
										<td>
											<span class="children_text">0</span>
										</td>
									</tr>
									<?php if ($enable_extra_items) { ?>
									<tr>
										<th>
											<?php pll_e('Tour booking total') ?>
										</th>
										<td>
											<span class="reservation_total"></span>
										</td>
									</tr>
									<tr class="extra_items_breakdown_row">
										<th>
											<?php pll_e('Extra items total') ?>
										</th>
										<td>
											<span class="extra_items_total"></span>
										</td>
									</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th><?php pll_e('Total price') ?></th>
										<td class="total_price"></td>
									</tr>
								</tfoot>
							</table>
							<a href="#" class="toggle_breakdown show_breakdown"><?php pll_e('Show price breakdown') ?></a>
							<div class="row price_breakdown_row hidden" style="display:none">
								<div class="f-item full-width">
									<label><?php pll_e('Tour price breakdown') ?></label>
									<table class="tour_price_breakdown tablesorter responsive">
										<thead></thead>
										<tbody></tbody>
										<tfoot></tfoot>
									</table>
								</div>
							</div>
							<div class="row price_breakdown_row extra_items_breakdown_row" style="display:none">
								<div class="f-item full-width">
									<label><?php pll_e('Extra items price breakdown') ?></label>
									<table class="extra_items_price_breakdown tablesorter responsive">
										<thead></thead>
										<tbody></tbody>
										<tfoot></tfoot>
									</table>
								</div>
							</div>
						</div>

					</div>
				<?php
					echo '<div class="booking-commands">';
					/*BookYourTravel_Theme_Utils::render_link_button("#", "gradient-button book-tour-reset", "book-tour-rest", pll__('Reset', 'bookyourtravel'));*/
					BookYourTravel_Theme_Utils::render_link_button("#", "clearfix gradient-button book-tour-proceed", "book-tour", pll__('Proceed', 'bookyourtravel'));
					echo '</div>';
				} else {
					echo '<div class="text-wrap">' . pll__('Unfortunately, no places are available on this tour at the moment', 'bookyourtravel') . '</div>';
				}
				?>
			</form>
			<?php BookYourTravel_Theme_Utils::render_tab_extra_fields('tour_extra_fields', $tour_extra_fields, 'availability', $tour_obj); ?>
			<?php do_action( 'bookyourtravel_show_single_tour_availability_after' ); ?>
		</article>
	</section>
	<!--//availability-->

	<?php if (!empty($tour_map_code)) { ?>
	<!--map-->
	<section id="map" class="tab-content <?php echo $first_display_tab == 'map' ? 'initial' : ''; ?>">
		<article>
			<?php do_action( 'bookyourtravel_show_single_tour_map_before' ); ?>
			<!--map-->
			<div class="gmap"><?php echo $tour_map_code; ?></div>
			<!--//map-->
			<?php BookYourTravel_Theme_Utils::render_tab_extra_fields('tour_extra_fields', $tour_extra_fields, 'map', $tour_obj); ?>
			<?php do_action( 'bookyourtravel_show_single_tour_map_after' ); ?>
		</article>
	</section>
	<!--//map-->
	<?php } // endif (!empty($tour_map_code)) ?>

	<?php if ($tour_locations && count($tour_locations) > 0) { ?>
	<!--locations-->
	<section id="locations" class="tab-content <?php echo $first_display_tab == 'locations' ? 'initial' : ''; ?>">
		<article>
			<?php do_action( 'bookyourtravel_show_single_tour_locations_before' ); ?>
			<?php foreach ($tour_locations as $location_id) {
				$location_obj = new BookYourTravel_Location((int)$location_id);
				$location_title = $location_obj->get_title();
				$location_excerpt = $location_obj->get_excerpt();
				if (!empty($location_title)) {
					BookYourTravel_Theme_Utils::render_field("", "", "", BookYourTravel_Theme_Utils::render_image('', '', $location_obj->get_main_image(), $location_title, $location_title, false) . $location_excerpt, $location_title);
					BookYourTravel_Theme_Utils::render_link_button(get_permalink($location_obj->get_id()), "gradient-button right", "", esc_html__('Read more', 'bookyourtravel'));
				}
			} ?>
			<?php BookYourTravel_Theme_Utils::render_tab_extra_fields('tour_extra_fields', $tour_extra_fields, 'locations', $tour_obj); ?>
			<?php do_action( 'bookyourtravel_show_single_tour_locations_after' ); ?>
		</article>
	</section>
	<!--//locations-->
	<?php } // endif (!empty($tour_map_code)) ?>
	<?php if ($enable_reviews) { ?>
	<!--reviews-->
	<section id="reviews" class="tab-content <?php echo $first_display_tab == 'review' ? 'initial' : ''; ?>">
		<?php
		do_action( 'bookyourtravel_show_single_tour_reviews_before' );
		get_template_part('includes/parts/review', 'item');
		BookYourTravel_Theme_Utils::render_tab_extra_fields('tour_extra_fields', $tour_extra_fields, 'reviews', $tour_obj);
		do_action( 'bookyourtravel_show_single_tour_reviews_after' );
		?>
	</section>
	<!--//reviews-->
	<?php } // if ($enable_reviews) ?>
	<!--Note-->
	<section id="note" class="tab-content <?php echo $first_display_tab == 'note' ? 'initial' : ''; ?>">
		<?php 
			$note_arr_index = 0;
			foreach ($tour_extra_fields as $key => $value) {
				if($value['id'] == 'note')
					break;
				$note_arr_index++;
			}
			BookYourTravel_Theme_Utils::render_tab_extra_fields('tour_extra_fields', $tour_extra_fields[$note_arr_index], 'description', $tour_obj);
		?>
	</section>
	<!--//Note-->
	<?php
	foreach ($tab_array as $tab) {
		if (count(BookYourTravel_Theme_Utils::custom_array_search($default_tour_tabs, 'id', $tab['id'])) == 0) {
			$all_empty_fields = BookYourTravel_Theme_Utils::are_tab_fields_empty('tour_extra_fields', $tour_extra_fields, $tab['id'], $tour_obj);

			if (!$all_empty_fields) {
		?>
			<section id="<?php echo esc_attr($tab['id']); ?>" class="tab-content <?php echo ($first_display_tab == $tab['id'] ? 'initial' : ''); ?>">
				<article>
					<?php do_action( 'bookyourtravel_show_single_tour_' . $tab['id'] . '_before' ); ?>
					<?php BookYourTravel_Theme_Utils::render_tab_extra_fields('tour_extra_fields', $tour_extra_fields, $tab['id'], $tour_obj); ?>
					<?php do_action( 'bookyourtravel_show_single_tour_' . $tab['id'] . '_after' ); ?>
				</article>
			</section>
		<?php
			}
		}
	}
	do_action( 'bookyourtravel_show_single_tour_tab_content_after' ); ?>
</section>
<!--//tour content-->
<?php
	get_sidebar('right-tour');
?>
</div>
<?php
} // end if
get_footer();

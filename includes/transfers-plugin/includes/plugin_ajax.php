<?php

class Transfers_Plugin_Ajax extends Transfers_BaseSingleton {

	protected function __construct() {

	    // our parent class might contain shared code in its constructor
        parent::__construct();
    }

    public function init() {

		add_action( 'wp_ajax_number_format_i18n_request', array( $this, 'number_format_i18n_request' ) );
		add_action( 'wp_ajax_nopriv_number_format_i18n_request', array( $this, 'number_format_i18n_request' ) );
		add_action( 'wp_ajax_book_transfer_ajax_request', array( $this, 'book_transfer_ajax_request') );
		add_action( 'wp_ajax_nopriv_book_transfer_ajax_request', array( $this, 'book_transfer_ajax_request') );

		add_action( 'wp_ajax_transfers_extra_tables_ajax_request', array( $this, 'transfers_extra_tables_ajax_request' ) );
		add_action( 'wp_ajax_nopriv_transfers_extra_tables_ajax_request', array( $this, 'transfers_extra_tables_ajax_request' ) );
		add_action( 'wp_ajax_generate_unique_dynamic_element_id', array( $this, 'generate_unique_dynamic_element_id' ) );
	}

	function generate_unique_dynamic_element_id() {

		if ( isset($_REQUEST) ) {

			$nonce = wp_kses($_REQUEST['nonce'], array());

			if ( wp_verify_nonce( $nonce, 'optionsframework-options' ) ) {

				global $transfers_plugin_globals;

				$element_type = sanitize_text_field($_REQUEST['element_type']);
				$parent = sanitize_text_field($_REQUEST['parent']);
				$element_id = trim(sanitize_text_field($_REQUEST['element_id']));

				if (empty($element_id) && $element_type == 'field') {
					$element_id = 'f';
				} else if (empty($element_id) && $element_type == 'booking_form_field') {
					$element_id = 'bff';
				}

				$elements = null;
				if ($parent == 'destination_extra_fields') {
					$elements = $transfers_plugin_globals->get_destination_extra_fields();
				} else if ($parent == 'booking_form_fields') {
					$elements = $transfers_plugin_globals->get_booking_form_fields();
				}

				$exists_count = 1;
				$new_element_id = $element_id;
				$exists = Transfers_Plugin_Of_Custom::of_element_exists($elements, $element_id);
				if ($exists) {
					while ($exists) {
						$new_element_id = $element_id . '_' . $exists_count;
						$exists = Transfers_Plugin_Of_Custom::of_element_exists($elements, $new_element_id);
						$exists_count++;
					}
				}

				echo json_encode($new_element_id);
			}
		}

		die();
	}

	function transfers_extra_tables_ajax_request() {

		if ( isset($_REQUEST) ) {

			$nonce = $_REQUEST['nonce'];

			if ( wp_verify_nonce( $nonce, 'optionsframework-options' ) ) {

				global $transfers_plugin_post_types;
				$transfers_plugin_post_types->create_extra_tables(true);
				echo "1";
			} else {
				echo "00";
			}

		} else {
			echo "-1";
		}

		die();
	}

	function book_transfer_ajax_request() {
		var_dump("ABC");
		wp_die();
	}

	function number_format_i18n_request() {

		if ( isset($_REQUEST) ) {

			$nonce = $_REQUEST['nonce'];

			if ( wp_verify_nonce( $nonce, 'transfers-ajax-nonce' ) ) {

				global $transfers_plugin_globals;

				$price_decimal_places = $transfers_plugin_globals->get_price_decimal_places();

				$number = floatval(wp_kses($_REQUEST['number'], ''));

				echo number_format_i18n( $number, $price_decimal_places );

			}
		}

		// Always die in functions echoing ajax content
		die();
	}

}

// store the instance in a variable to be retrieved later and call init
$transfers_plugin_ajax = Transfers_Plugin_Ajax::get_instance();
$transfers_plugin_ajax->init();
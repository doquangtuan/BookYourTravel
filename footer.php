<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content
 *
 * @package WordPress
 * @subpackage BookYourTravel
 * @since Book Your Travel 1.0
 */
global $bookyourtravel_theme_globals;
?><!--//main content-->
</div><!--//wrap-->
<?php get_sidebar('above-footer'); ?>
</div><!--//main-->
<!--footer-->
<footer class="footer" role="contentinfo">
	<?php get_sidebar('footer'); ?>
	<div class="wrap">
		<div class="row">
			<div class="full-width">
				<p class="copy"><?php echo esc_html($bookyourtravel_theme_globals->get_copyright_footer()); ?></p>							
			</div>
		</div>
	</div>
</footer>
<!--//footer-->
<?php 

get_template_part('includes/parts/login', 'lightbox');
get_template_part('includes/parts/register', 'lightbox'); 
wp_footer();
if (WP_DEBUG) {
	$num_queries = get_num_queries();
	$timer = timer_stop(0);
	echo '<!-- ' . $num_queries . ' queries in ' . $timer . ' seconds. -->';
} 
?>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/580d8085c7829d0cd36aec37/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->	
</body>
</html>